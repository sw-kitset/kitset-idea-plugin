package org.swkitset.kitsetIdeaPlugin

import com.intellij.execution.configurations.GeneralCommandLine
import com.intellij.execution.process.OSProcessHandler
import com.intellij.execution.process.ProcessAdapter
import com.intellij.execution.process.ProcessEvent
import com.intellij.execution.process.ProcessOutputTypes
import com.intellij.openapi.project.ex.ProjectManagerEx
import com.intellij.openapi.util.Key
import com.intellij.openapi.wm.StatusBar
import kotlinx.coroutines.*
import java.nio.file.Path

internal fun fetchLanguages(): Array<String> {
    val tmp = mutableListOf<String>()
    val kitsetHome = ProcessBuilder().environment()["KITSET_HOME"]
    val process = ProcessBuilder()
        .command("$kitsetHome/kitset.kexe", "List Languages", "-s")
        .start()
    process.inputStream.bufferedReader().use { br ->
        br.lines().forEach { line -> tmp += line }
    }
    return tmp.toTypedArray()
}

internal fun fetchKitsets(lang: String): Array<String> {
    val tmp = mutableListOf<String>()
    val kitsetHome = ProcessBuilder().environment()["KITSET_HOME"]
    val process = ProcessBuilder()
        .command("$kitsetHome/kitset.kexe", "List Kitsets", lang, "-s")
        .start()
    process.inputStream.bufferedReader().use { br ->
        br.lines().forEach { line -> tmp += line }
    }
    return tmp.toTypedArray()
}

internal fun CoroutineScope.openIfProjectUsesGradle(
    name: String,
    parentDir: String,
    lang: String,
    type: String
) {
    val kitsetHome = ProcessBuilder().environment()["KITSET_HOME"]
    val cmd = GeneralCommandLine()
        .withExePath("$kitsetHome/kitset.kexe")
        .withParameters("Kitset Uses Gradle", lang, type, "-s")
    val processHandler = OSProcessHandler(cmd)
    processHandler.addProcessListener(object : ProcessAdapter() {
        override fun onTextAvailable(evt: ProcessEvent, outputType: Key<*>) {
            if (outputType == ProcessOutputTypes.STDOUT) {
                if (evt.text.trim().toBoolean()) {
                    launch(Dispatchers.Main) { openProject("$parentDir/$name") }
                }
            }
        }
    })
    processHandler.startNotify()
}

private fun openProject(path: String) {
    // TODO: Figure out how to import a Gradle project into the IDE.
    val pm = ProjectManagerEx.getInstanceEx()
    val project = pm.loadProject(Path.of(path))
    pm.openProject(project)
    // Change the text in the Status Bar.
    StatusBar.Info.set("Opened project.", null)
}
