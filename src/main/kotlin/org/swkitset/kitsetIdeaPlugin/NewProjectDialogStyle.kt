package org.swkitset.kitsetIdeaPlugin

import java.awt.Dimension
import java.awt.Font

internal object NewProjectDialogStyle {
    val langListPreferredSize = Dimension(250, 120)
    val typeListPreferredSize = langListPreferredSize
    val nameLblFont = Font("Serif", Font.BOLD, 16)
    val parentDirLblFont = nameLblFont
    const val parentDirTxtColumns = 30
    val langLblFont = nameLblFont
    val typeLblFont = nameLblFont
    const val WIDTH = 800
    const val HEIGHT = 600
    val titleLblFont = Font("Serif", Font.BOLD, 30)
}
