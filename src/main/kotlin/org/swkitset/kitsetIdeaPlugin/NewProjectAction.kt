package org.swkitset.kitsetIdeaPlugin

import com.intellij.execution.configurations.GeneralCommandLine
import com.intellij.execution.process.OSProcessHandler
import com.intellij.openapi.actionSystem.AnAction
import com.intellij.openapi.actionSystem.AnActionEvent
import com.intellij.openapi.ui.Messages
import com.intellij.openapi.wm.StatusBar
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext


class NewProjectAction : AnAction() {
    override fun update(evt: AnActionEvent) {
        super.update(evt)
        evt.presentation.isEnabledAndVisible = true
    }

    override fun actionPerformed(evt: AnActionEvent) {
        val os = System.getProperty("os.name")
        val msg = "The $os OS isn't supported!"
        val title = "Unsupported OS"
        if (os != "Linux") {
            Messages.showMessageDialog(null, msg, title, Messages.getErrorIcon())
        } else {
            showDialog()
        }
    }

    private fun showDialog() = GlobalScope.launch(Dispatchers.Main) {
        val dlg = NewProjectDialog()
        if (dlg.showAndGet()) {
            // Change the text in the Status Bar.
            StatusBar.Info.set("Creating project using the ${dlg.selectedLang} ${dlg.selectedType} Kitset...", null)
            createProject(name = dlg.projectName, parentDir = dlg.projectParentDir, lang = dlg.selectedLang,
                type = dlg.selectedType)
        }
    }

    @Suppress("RedundantSuspendModifier")
    private suspend fun createProject(
        name: String,
        parentDir: String,
        lang: String,
        type: String
    ) = withContext(Dispatchers.Default) {
        val kitsetHome = ProcessBuilder().environment()["KITSET_HOME"]
        val cmd = GeneralCommandLine().withExePath("$kitsetHome/kitset.kexe").withParameters(
            "Generate Project",
            lang,
            type,
            name,
            parentDir
        )
        OSProcessHandler(cmd).startNotify()
        openIfProjectUsesGradle(name = name, parentDir = parentDir, lang = lang, type = type)
    }
}
