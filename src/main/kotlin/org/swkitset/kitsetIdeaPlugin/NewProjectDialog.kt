package org.swkitset.kitsetIdeaPlugin

import com.intellij.openapi.fileChooser.FileChooser
import com.intellij.openapi.fileChooser.FileChooserDescriptorFactory
import com.intellij.openapi.ui.DialogWrapper
import com.intellij.openapi.ui.ValidationInfo
import com.intellij.ui.components.JBList
import com.intellij.ui.components.JBScrollPane
import com.intellij.ui.layout.panel
import java.awt.event.ActionEvent
import javax.swing.*

class NewProjectDialog : DialogWrapper(null, true, IdeModalityType.IDE) {
    private val style = NewProjectDialogStyle
    private lateinit var titleLbl: JLabel
    private lateinit var nameLbl: JLabel
    private lateinit var parentDirLbl: JLabel
    private lateinit var langLbl: JLabel
    private lateinit var typeLbl: JLabel
    private lateinit var nameTxt: JTextField
    private lateinit var parentDirTxt: JTextField
    private val langListModel = DefaultListModel<String>()
    private val typeListModel = DefaultListModel<String>()
    private lateinit var langListScroller: JBScrollPane
    private val langList = JBList(langListModel)
    private lateinit var typeListScroller: JBScrollPane
    private val typeList = JBList(typeListModel)
    val selectedLang: String
        get() {
            val selectedIndices = langList.selectionModel.selectedIndices
            val pos = if (selectedIndices.isNotEmpty()) langList.selectionModel.selectedIndices.first() else -1
            return if (pos >= 0) langListModel.getElementAt(pos) else ""
        }
    val selectedType: String
        get() {
            val selectedIndices = typeList.selectionModel.selectedIndices
            val pos = if (selectedIndices.isNotEmpty()) typeList.selectionModel.selectedIndices.first() else -1
            return if (pos >= 0) typeListModel.getElementAt(pos) else ""
        }
    var projectName = ""
    var projectParentDir = ""

    init {
        title = "New Project From Kitset"
        setSize(style.WIDTH, style.HEIGHT)
        init()
        titleLbl.font = style.titleLblFont
        nameLbl.font = style.nameLblFont
        parentDirLbl.font = style.parentDirLblFont
        langLbl.font = style.langLblFont
        typeLbl.font = style.typeLblFont

        langListScroller.preferredSize = style.langListPreferredSize
        typeListScroller.preferredSize = style.typeListPreferredSize
        typeList.selectionMode = ListSelectionModel.SINGLE_SELECTION
        parentDirTxt.columns = style.parentDirTxtColumns
        setupLangList()
        setOKButtonText("Create")
        nameTxt.requestFocusInWindow()
    }

    override fun doValidate(): ValidationInfo? = when {
        nameTxt.text.isEmpty() -> ValidationInfo("Name cannot be empty")
        parentDirTxt.text.isEmpty() -> ValidationInfo("Parent Directory cannot be empty")
        selectedLang.isEmpty() -> ValidationInfo("A Language must be selected")
        selectedType.isEmpty() -> ValidationInfo("A Type must be selected")
        else -> null
    }

    private fun setupLangList() {
        langList.selectionMode = ListSelectionModel.SINGLE_SELECTION
        langList.selectionModel.addListSelectionListener { evt ->
            val src = (evt.source as ListSelectionModel)
            populateTypeList(langList.model.getElementAt(src.selectedIndices.first()))
        }
        populateLangList()
    }

    private fun populateTypeList(lang: String) {
        with(typeListModel) {
            clear()
            val tmp = mutableListOf<String>()
            tmp.addAll(fetchKitsets(lang))
            addAll(tmp)
        }
    }

    private fun populateLangList() {
        val tmp = mutableListOf<String>()
        tmp.addAll(fetchLanguages())
        langListModel.addAll(tmp)
    }

    override fun createCenterPanel(): JComponent? {
        return panel {
            row {
                titleLbl = label("New Project From Kitset").component
            }
            row {
                cell { nameLbl = label("Name:").component }
                cell { nameTxt = textField(::projectName).component }
            }
            row {
                cell { parentDirLbl = label("Parent Directory:").component }
                cell { parentDirTxt = textField(::projectParentDir).component }
                cell { button("...", parentDirBtnAction()) }
            }
            row {
                cell { langLbl = label("Language").component }
                cell { typeLbl = label("Type").component }
            }
            row {
                cell { langListScroller = component(JBScrollPane(langList)).component }
                cell { typeListScroller = component(JBScrollPane(typeList)).component }
            }
        }
    }

    private fun parentDirBtnAction() = { evt: ActionEvent ->
        val descriptor = FileChooserDescriptorFactory.createSingleFolderDescriptor()
        val dirChooser = FileChooser.chooseFile(descriptor, null, null)
        if (dirChooser?.path != null) parentDirTxt.text = dirChooser.path
    }
}
