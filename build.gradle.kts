import org.jetbrains.intellij.tasks.PatchPluginXmlTask
import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    id("org.jetbrains.intellij") version "0.4.26"
    kotlin("jvm") version "1.4.10"
}

group = "org.swkitset"
version = "0.1-SNAPSHOT"

repositories {
    jcenter()
    mavenCentral()
}

dependencies {
    val kotlinVer = "1.4.10"
    val coroutinesVer = "1.3.9"
    implementation(kotlin("stdlib-jdk8", kotlinVer))
    implementation(kotlin("reflect", kotlinVer))
    implementation("org.jetbrains.kotlinx:kotlinx-coroutines-swing:$coroutinesVer")
}

// See https://github.com/JetBrains/gradle-intellij-plugin/
intellij {
    version = "2020.1.4"
    pluginName = "Kitset"
}

tasks.withType<KotlinCompile>().configureEach {
    kotlinOptions.jvmTarget = "1.8"
}

tasks.withType<JavaCompile> {
    sourceCompatibility = "1.8"
    targetCompatibility = "1.8"
}

tasks.getByName<PatchPluginXmlTask>("patchPluginXml") {
    changeNotes(
        """
        Add change notes here.<br>
        <em>most HTML tags may be used</em>
        """
    )
}